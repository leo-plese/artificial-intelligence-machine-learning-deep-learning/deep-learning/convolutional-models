import torch
from torch import nn
from torch.optim import SGD
from torch.optim.lr_scheduler import StepLR

import time
from pathlib import Path

import os
import math

import numpy as np
import skimage as ski
import skimage.io
import matplotlib.pyplot as plt

from torchvision.datasets import MNIST

DATA_DIR = Path(__file__).parent / 'datasets'  # / 'MNIST'
SAVE_DIR = Path(__file__).parent / 'out'
SAVE_TRAIN_LOSS_DIR = Path(__file__).parent / 'losses' / 'train'
SAVE_VAL_LOSS_DIR = Path(__file__).parent / 'losses' / 'val'
SAVE_TRAIN_ACC_DIR = Path(__file__).parent / 'accuracy' / 'train'
SAVE_VAL_ACC_DIR = Path(__file__).parent / 'accuracy' / 'val'

config = {}
config['max_epochs'] = 8
config['batch_size'] = 50
config['save_dir'] = SAVE_DIR
config['weight_decay'] = 0 # 1e-3 # 1e-2 # 1e-1 # 0
config['lr'] = 1e-1
config['save_train_loss_dir'] = SAVE_TRAIN_LOSS_DIR / (str(config['weight_decay'])+".txt")
config['save_val_loss_dir'] = SAVE_VAL_LOSS_DIR / (str(config['weight_decay'])+".txt")
config['save_train_acc_dir'] = SAVE_TRAIN_ACC_DIR / (str(config['weight_decay'])+".txt")
config['save_val_acc_dir'] = SAVE_VAL_ACC_DIR / (str(config['weight_decay'])+".txt")

weight_decay = config['weight_decay']
print("weight_decay =",weight_decay)

def dense_to_one_hot(y, class_count):
    return torch.eye(class_count)[y]

# np.random.seed(100)
rnd_seed = int(time.time() * 1e6) % 2 ** 31
np.random.seed(rnd_seed)
torch.random.manual_seed(rnd_seed)

ds_train, ds_test = MNIST(DATA_DIR, train=True, download=False), MNIST(DATA_DIR, train=False)

train_x, train_y = ds_train.data.reshape((-1, 1, 28, 28)), ds_train.targets
test_x, test_y = ds_test.data.reshape((-1, 1, 28, 28)), ds_test.targets

train_x, test_x = train_x.float().div_(255.0), test_x.float().div_(255.0)

train_x, valid_x = train_x[:55000], train_x[55000:]
train_y, valid_y = train_y[:55000], train_y[55000:]

train_y_orig, valid_y_orig, test_y_orig = train_y, valid_y, test_y

train_mean = train_x.mean()
train_x, valid_x, test_x = (x - train_mean for x in (train_x, valid_x, test_x))
train_y, valid_y, test_y = (dense_to_one_hot(y, 10) for y in (train_y, valid_y, test_y))


class ConvolutionalModel(nn.Module):
    def __init__(self, conv1_in_channels, conv1_width, conv1_hw, conv1_stride, conv1_pad, conv1_bias, pool1_hw, pool1_stride, conv2_width, conv2_hw, conv2_stride, conv2_pad, conv2_bias, pool2_hw, pool2_stride,
                fc1_in_features, fc1_width, fc1_bias, class_count, fc2_bias):
        super().__init__()
        #  konvolucijski slojevi i slojevi sažimanja
        self.conv1 = nn.Conv2d(conv1_in_channels, conv1_width, kernel_size=conv1_hw, stride=conv1_stride, padding=conv1_hw//2 if conv1_pad else 0, bias=conv1_bias)
        self.pool1 = nn.MaxPool2d(pool1_hw, pool1_stride)
        self.conv2 = nn.Conv2d(conv1_width, conv2_width, kernel_size=conv2_hw, stride=conv2_stride, padding=conv2_hw//2 if conv2_pad else 0, bias=conv2_bias)
        self.pool2 =nn.MaxPool2d(pool2_hw, pool2_stride)

        # potpuno povezani slojevi
        self.fc1 = nn.Linear(fc1_in_features, fc1_width, bias=fc1_bias)
        self.fc_logits = nn.Linear(fc1_width, class_count, bias=fc2_bias)

        # parametri su već inicijalizirani pozivima Conv2d i Linear
        # ali možemo ih drugačije inicijalizirati
        self.reset_parameters()

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear) and m is not self.fc_logits:
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc_logits.reset_parameters()

    def forward(self, x):
        h = self.conv1(x)
        h = self.pool1(h)
        h = torch.relu(h)  # može i h.relu() ili nn.functional.relu(h)

        h = self.conv2(h)
        h = self.pool2(h)
        h = torch.relu(h)

        h = h.view(h.shape[0], -1)      # flatten
        h = self.fc1(h)
        h = torch.relu(h)
        logits = self.fc_logits(h)

        return logits

    def draw_conv_filters(self, epoch, layer, save_dir):
        C = layer.in_channels
        w = layer.weight.cpu().detach().numpy().copy()
        num_filters = w.shape[0]
        k = int(np.sqrt(w.size/len(w) / C))
        w = w.reshape(num_filters, C, k, k)
        w -= w.min()
        w /= w.max()
        border = 1
        cols = 8
        rows = math.ceil(num_filters / cols)
        width = cols * k + (cols - 1) * border
        height = rows * k + (rows - 1) * border


        img = np.zeros([height, width])
        for j in range(num_filters):
            r = int(j / cols) * (k + border)
            c = int(j % cols) * (k + border)
            img[r:r + k, c:c + k] = w[j, 0]
        filename = '%s_epoch_%02d.png' % ("conv1", epoch)
        ski.io.imsave(os.path.join(save_dir, filename), img)

    def eval_model(self, name, x, y, valid_y_orig, batch_size):
        print("\nRunning evaluation: ", name)
        num_examples = x.shape[0]
        assert num_examples % batch_size == 0
        num_batches = num_examples // batch_size
        cnt_correct = 0
        loss_avg = 0
        for i in range(num_batches):
            batch_x = x[i * batch_size:(i + 1) * batch_size, :]
            batch_y = y[i * batch_size:(i + 1) * batch_size, :]
            logits = self.forward(batch_x)

            yp = torch.argmax(logits, 1)
            yt = torch.argmax(batch_y, 1)
            cnt_correct += (yp == yt).sum()

            loss = nn.CrossEntropyLoss()
            loss_val = loss(logits, valid_y_orig[i * batch_size:(i + 1) * batch_size])
            loss_avg += loss_val

        valid_acc = cnt_correct / num_examples * 100
        loss_avg /= num_batches
        print(name + " accuracy = %.2f" % valid_acc)
        print(name + " avg loss = %.2f\n" % loss_avg)

        return valid_acc, loss_avg

    def train_model(self, train_x, train_y, valid_x, valid_y, train_y_orig, valid_y_orig, config):
        batch_size = config['batch_size']
        max_epochs = config['max_epochs']
        save_dir = config['save_dir']
        init_lr = config['lr']

        self.draw_conv_filters(0, self.conv1, save_dir)

        optimizer = SGD(self.parameters(), lr=init_lr, weight_decay=config['weight_decay'])
        scheduler = StepLR(optimizer, step_size=2, gamma=0.1)


        num_examples = train_x.shape[0]
        assert num_examples % batch_size == 0
        num_batches = num_examples // batch_size

        train_loss_list, val_loss_list = [], []
        train_acc_list, val_acc_list = [], []

        for epoch in range(1, max_epochs + 1):
            print()
            print("######################## EPOCH", epoch, " ########################")

            train_inds = torch.randperm(num_examples)
            train_x, train_y = train_x[train_inds], train_y[train_inds]
            train_y_orig = train_y_orig[train_inds]

            # train
            print("********* TRAIN *********")

            cnt_correct = 0
            train_loss = 0
            for i in range(num_batches):
                optimizer.zero_grad()

                batch_x = train_x[i * batch_size:(i + 1) * batch_size, :]
                batch_y = train_y[i * batch_size:(i + 1) * batch_size, :]

                logits = self.forward(batch_x)

                loss = nn.CrossEntropyLoss()
                loss_val = loss(logits, train_y_orig[i * batch_size:(i + 1) * batch_size])

                train_loss += loss_val

                loss_val.backward()
                optimizer.step()

                yp = torch.argmax(logits, 1)
                yt = torch.argmax(batch_y, 1)
                cnt_correct += (yp == yt).sum()

                if i % 5 == 0:
                    print("epoch %d, step %d/%d, batch loss = %.2f" % (epoch, i * batch_size, num_examples, loss_val))
                if i > 0 and i % 50 == 0:

                    print("Train accuracy = %.2f" % (cnt_correct / ((i + 1) * batch_size) * 100))

            train_loss /= num_batches
            train_acc = cnt_correct / num_examples * 100

            train_loss_list.append(train_loss)
            train_acc_list.append(train_acc)

            self.draw_conv_filters(epoch, self.conv1, save_dir)

            print("Train loss = %.2f" % (train_loss))
            print("Train accuracy = %.2f" % (train_acc))

            # val
            print()
            print("********* VAL *********")
            with torch.no_grad():
                valid_acc, valid_loss = self.eval_model("Validation", valid_x, valid_y, valid_y_orig, batch_size)

                val_loss_list.append(valid_loss)
                val_acc_list.append(valid_acc)

            scheduler.step()

        np.savetxt(config['save_train_loss_dir'], np.array(train_loss_list))
        np.savetxt(config['save_train_acc_dir'], np.array(train_acc_list))

        np.savetxt(config['save_val_loss_dir'], np.array(val_loss_list))
        np.savetxt(config['save_val_acc_dir'], np.array(val_acc_list))

        return train_loss_list, train_acc_list, val_loss_list, val_acc_list





conv_net = ConvolutionalModel(conv1_in_channels=1, conv1_width=16, conv1_hw=5, conv1_stride=1, conv1_pad=True, conv1_bias=True, pool1_hw=2, pool1_stride=2, conv2_width=32, conv2_hw=5, conv2_stride=1, conv2_pad=True, conv2_bias=True, pool2_hw=2, pool2_stride=2,
                              fc1_in_features=1568, fc1_width=512, fc1_bias=True, class_count=10, fc2_bias=True).cuda()

train_loss_list, train_acc_list, val_loss_list, val_acc_list = conv_net.train_model(train_x.cuda(), train_y.cuda(), valid_x.cuda(), valid_y.cuda(), train_y_orig.cuda(), valid_y_orig.cuda(), config)
plt.plot(range(1, config['max_epochs']+1), train_loss_list, label="Train")
plt.plot(range(1, config['max_epochs']+1), val_loss_list, label="Val")
plt.legend(loc="best")
plt.title("Loss")
plt.xlabel("n iter")
plt.ylabel("loss")
plt.show()

plt.plot(range(1, config['max_epochs']+1), train_acc_list, label="Train")
plt.plot(range(1, config['max_epochs']+1), val_acc_list, label="Val")
plt.legend(loc="best")
plt.title("Accuracy")
plt.xlabel("n iter")
plt.ylabel("accuracy")
plt.show()


print()
print("****************************** TEST ******************************")
conv_net.eval()
with torch.no_grad():
    conv_net.eval_model("Test", test_x.cuda(), test_y.cuda(), test_y_orig.cuda(), config['batch_size'])