import os
import pickle
import numpy as np
import torch
from torch import nn
from torch.optim import SGD
from torch.optim.lr_scheduler import ExponentialLR
import math
import skimage as ski
import skimage.io
import time
from pathlib import Path
import matplotlib.pyplot as plt

torch.backends.cudnn.enabled = False

def shuffle_data(data_x, data_y, data_y_orig=None):
  indices = np.arange(data_x.shape[0])
  np.random.shuffle(indices)
  shuffled_data_x = np.ascontiguousarray(data_x[indices])
  shuffled_data_y = np.ascontiguousarray(data_y[indices])
  if data_y_orig is not None:
      shuffled_data_y_orig = np.ascontiguousarray(data_y_orig[indices])
      return shuffled_data_x, shuffled_data_y, shuffled_data_y_orig
  return shuffled_data_x, shuffled_data_y

def unpickle(file):
  fo = open(file, 'rb')
  dict = pickle.load(fo, encoding='latin1')
  fo.close()
  return dict

def draw_image(img, mean, std, img_num, img_pred_label, img_gt_label):
  img = img.transpose(1, 2, 0)
  img *= std
  img += mean
  img = img.astype(np.uint8)

  ski.io.imshow(img)
  ski.io.show()

  filename = 'num_%02d_p_%s_gt_%s.png' % (img_num, img_pred_label, img_gt_label)
  ski.io.imsave(os.path.join(SAVE_DIR_TOP_WORST, filename), img)

def dense_to_one_hot(y, class_count):
    return np.eye(class_count)[y]

NUM_TO_NAME_CLASS_DICT = {0:"airplane", 1:"automobile", 2:"bird", 3:"cat", 4:"deer", 5:"dog", 6:"frog", 7:"horse", 8:"ship", 9:"truck"}

DATA_DIR = r'.\cifar-10-python\cifar-10-batches-py'
SAVE_DIR = Path(__file__).parent / 'out'
SAVE_DIR_TOP_WORST = Path(__file__).parent / 'cifar' / 'top20'

SAVE_LR_DIR = Path(__file__).parent / 'cifar' / 'lr'
SAVE_TRAIN_LOSS_DIR = Path(__file__).parent / 'cifar' / 'losses' / 'train'
SAVE_VAL_LOSS_DIR = Path(__file__).parent / 'cifar' / 'losses' / 'val'
SAVE_TRAIN_ACC_DIR = Path(__file__).parent / 'cifar' / 'accuracy' / 'train'
SAVE_VAL_ACC_DIR = Path(__file__).parent / 'cifar' / 'accuracy' / 'val'

SAVE_TRAIN_CONF_MAT_DIR = Path(__file__).parent / 'cifar' / 'confMat' / 'train'
SAVE_VAL_CONF_MAT_DIR = Path(__file__).parent / 'cifar' / 'confMat' / 'val'
SAVE_TRAIN_CONF_MAT_BY_CLS_DIR = Path(__file__).parent / 'cifar' / 'confMatByCls' / 'train'
SAVE_VAL_CONF_MAT_BY_CLS_DIR = Path(__file__).parent / 'cifar' / 'confMatByCls' / 'val'
SAVE_TRAIN_PREC_DIR = Path(__file__).parent / 'cifar' / 'precision' / 'train'
SAVE_VAL_PREC_DIR = Path(__file__).parent / 'cifar' / 'precision' / 'val'
SAVE_TRAIN_REC_DIR = Path(__file__).parent / 'cifar' / 'recall' / 'train'
SAVE_VAL_REC_DIR = Path(__file__).parent / 'cifar' / 'recall' / 'val'

SAVE_TRAIN_ACC_BY_CLS_DIR = Path(__file__).parent / 'cifar' / 'accuracyByCls' / 'train'
SAVE_VAL_ACC_BY_CLS_DIR = Path(__file__).parent / 'cifar' / 'accuracyByCls' / 'val'
SAVE_TRAIN_PREC_BY_CLS_DIR = Path(__file__).parent / 'cifar' / 'precisionByCls' / 'train'
SAVE_VAL_PREC_BY_CLS_DIR = Path(__file__).parent / 'cifar' / 'precisionByCls' / 'val'
SAVE_TRAIN_REC_BY_CLS_DIR = Path(__file__).parent / 'cifar' / 'recallByCls' / 'train'
SAVE_VAL_REC_BY_CLS_DIR = Path(__file__).parent / 'cifar' / 'recallByCls' / 'val'

config = {}
config['max_epochs'] = 50
config['batch_size'] = 50
config['save_dir'] = SAVE_DIR
config['lr_gamma'] = 0.95
config['weight_decay'] = 0 # 1e-3 # 1e-2 # 1e-1 # 0
config['lr'] = 1e-2

config['save_lr_dir'] = SAVE_LR_DIR / (str(config['weight_decay'])+".txt")
config['save_train_loss_dir'] = SAVE_TRAIN_LOSS_DIR / (str(config['weight_decay'])+".txt")
config['save_val_loss_dir'] = SAVE_VAL_LOSS_DIR / (str(config['weight_decay'])+".txt")
config['save_train_acc_dir'] = SAVE_TRAIN_ACC_DIR / (str(config['weight_decay'])+".txt")
config['save_val_acc_dir'] = SAVE_VAL_ACC_DIR / (str(config['weight_decay'])+".txt")

config['save_train_conf_mat_dir'] = lambda ep : SAVE_TRAIN_CONF_MAT_DIR / ("ep_"+str(ep)+"_"+str(config['weight_decay'])+".txt")
config['save_val_conf_mat_dir'] =  lambda ep : SAVE_VAL_CONF_MAT_DIR / ("ep_"+str(ep)+"_"+str(config['weight_decay'])+".txt")
config['save_train_conf_mat_by_cls_dir'] = SAVE_TRAIN_CONF_MAT_BY_CLS_DIR / (str(config['weight_decay'])+".txt")
config['save_val_conf_mat_by_cls_dir'] = SAVE_VAL_CONF_MAT_BY_CLS_DIR / (str(config['weight_decay'])+".txt")
config['save_train_prec_dir'] = SAVE_TRAIN_PREC_DIR / (str(config['weight_decay'])+".txt")
config['save_val_prec_dir'] = SAVE_VAL_PREC_DIR / (str(config['weight_decay'])+".txt")
config['save_train_rec_dir'] = SAVE_TRAIN_REC_DIR / (str(config['weight_decay'])+".txt")
config['save_val_rec_dir'] = SAVE_VAL_REC_DIR / (str(config['weight_decay'])+".txt")

config['save_train_acc_by_cls_dir'] = SAVE_TRAIN_ACC_BY_CLS_DIR / (str(config['weight_decay'])+".txt")
config['save_val_acc_by_cls_dir'] = SAVE_VAL_ACC_BY_CLS_DIR / (str(config['weight_decay'])+".txt")
config['save_train_prec_by_cls_dir'] = SAVE_TRAIN_PREC_BY_CLS_DIR / (str(config['weight_decay'])+".txt")
config['save_val_prec_by_cls_dir'] = SAVE_VAL_PREC_BY_CLS_DIR / (str(config['weight_decay'])+".txt")
config['save_train_rec_by_cls_dir'] = SAVE_TRAIN_REC_BY_CLS_DIR / (str(config['weight_decay'])+".txt")
config['save_val_rec_by_cls_dir'] = SAVE_VAL_REC_BY_CLS_DIR / (str(config['weight_decay'])+".txt")



rnd_seed = int(time.time() * 1e6) % 2 ** 31
np.random.seed(rnd_seed)
torch.random.manual_seed(rnd_seed)

img_height = 32
img_width = 32
num_channels = 3
num_classes = 10

train_x = np.ndarray((0, img_height * img_width * num_channels), dtype=np.float32)
train_y = []
for i in range(1, 6):
  subset = unpickle(os.path.join(DATA_DIR, 'data_batch_%d' % i))
  train_x = np.vstack((train_x, subset['data']))
  train_y += subset['labels']
train_x = train_x.reshape((-1, num_channels, img_height, img_width)).transpose(0, 2, 3, 1)
train_y = np.array(train_y, dtype=np.int64)

subset = unpickle(os.path.join(DATA_DIR, 'test_batch'))
test_x = subset['data'].reshape((-1, num_channels, img_height, img_width)).transpose(0, 2, 3, 1).astype(np.float32)
test_y = np.array(subset['labels'], dtype=np.int64)

valid_size = 5000
train_x, train_y = shuffle_data(train_x, train_y)
valid_x = train_x[:valid_size, ...]
valid_y = train_y[:valid_size, ...]
train_x = train_x[valid_size:, ...]
train_y = train_y[valid_size:, ...]

train_y_orig, valid_y_orig, test_y_orig = train_y, valid_y, test_y

data_mean = train_x.mean((0, 1, 2))
data_std = train_x.std((0, 1, 2))

train_x = (train_x - data_mean) / data_std
valid_x = (valid_x - data_mean) / data_std
test_x = (test_x - data_mean) / data_std

train_x = train_x.transpose(0, 3, 1, 2)
valid_x = valid_x.transpose(0, 3, 1, 2)
test_x = test_x.transpose(0, 3, 1, 2)

train_y, valid_y, test_y = (dense_to_one_hot(y, 10) for y in (train_y, valid_y, test_y))


print("weight_decay =",config['weight_decay'])

print()

########## EVAL METRICS ##########
def get_accuracy(tp, tn, N):
    return (tp + tn) / N

def get_recall(tp, fn):
    return tp / (tp + fn)

def get_precision(tp, fp):
    if (tp + fp) == 0:
        return None
    return tp / (tp + fp)

# actual x predicted
# classes: from 0 to C-1 (top to bottom (predicted), left to right (actual))
def get_confusion_matrix(Y, Y_, C):
    conf_mat = np.zeros((C,C))
    for i in range(C):
        Y_pred_i = Y[np.where(Y_ == i)]

        for x in Y_pred_i:
            conf_mat[i][x] += 1

    conf_mat = conf_mat.T

    return conf_mat

# c_ind - class index
# classes: 0, 1 (top to bottom (predicted), left to right (actual))
def get_conf_mat_for_class(multi_conf_mat, c_ind, C):

    tp = multi_conf_mat[c_ind][c_ind]
    tn = sum([multi_conf_mat[i][j] if i != c_ind and j != c_ind else 0 for i in range(C) for j in range(C)])
    fp = sum([multi_conf_mat[c_ind][i] if i != c_ind else 0 for i in range(C)])
    fn = sum([multi_conf_mat[i][c_ind] if i != c_ind else 0 for i in range(C)])

    conf_mat = np.array([[tn,fn],[fp, tp]])

    return conf_mat

def eval_perf_multi(Y, Y_, N=None, C=None):
    if not C:
        C = max(Y_)[0] + 1  # 3
    if not N:
        N = len(Y_)
    conf_mat = get_confusion_matrix(Y.flatten(), Y_.flatten(), C)
    # print(conf_mat)

    conf_mats_by_cls = np.zeros((C, 2, 2))
    accuracy_by_cls, precision_by_cls, recall_by_cls = np.zeros(C), np.zeros(C), np.zeros(C)
    for i in range(C):
        conf_mat_for_class_i = get_conf_mat_for_class(conf_mat, i, C)
        conf_mats_by_cls[i] = conf_mat_for_class_i.T

        accuracy_by_cls[i] = get_accuracy(conf_mat_for_class_i[1][1], conf_mat_for_class_i[0][0], N)
        precision_by_cls[i] = get_precision(conf_mat_for_class_i[1][1], conf_mat_for_class_i[1][0])
        recall_by_cls[i] = get_recall(conf_mat_for_class_i[1][1], conf_mat_for_class_i[0][1])

    accuracy_avg = np.mean(accuracy_by_cls)
    precision_avg = np.mean(precision_by_cls)
    recall_avg = np.mean(recall_by_cls)

    accuracy_score = np.mean(Y_.reshape(-1,1)==Y)


    return conf_mat.T, conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls

def evaluate(model, X, Y_, N, C, batch_size):
    #torch.cuda.empty_cache()
    with torch.no_grad():
        num_batches = N // batch_size

        logits_list = []

        for i in range(num_batches):
            batch_x = X[i * batch_size:(i + 1) * batch_size, :]
            batch_logits = model.forward(batch_x)
            logits_list.append(batch_logits)

        logits = torch.cat(logits_list, dim=0)

        #logits = model.forward(X)
        probs = torch.softmax(logits, dim=1).cpu().detach().numpy()
        # recover the predicted classes Y
        Y = np.argmax(probs, axis=1).reshape((N, -1))

        return eval_perf_multi(Y, Y_.cpu().detach().numpy(), N, C)


class ConvolutionalModel(nn.Module):
    def __init__(self):
        super().__init__()
        #  konvolucijski slojevi i slojevi sažimanja
        self.conv1 = nn.Conv2d(3, 16, kernel_size=5, stride=1, padding=5//2, bias=True)
        self.pool1 = nn.MaxPool2d(3, 2)
        self.conv2 = nn.Conv2d(16, 32, kernel_size=5, stride=1, padding=5 // 2, bias=True)
        self.pool2 = nn.MaxPool2d(3, 2)

        # potpuno povezani slojevi
        self.fc1 = nn.Linear(1568, 256, bias=True)
        self.fc2 = nn.Linear(256, 128, bias=True)
        self.fc_logits = nn.Linear(128, 10, bias=True)

        # parametri su već inicijalizirani pozivima Conv2d i Linear
        # ali možemo ih drugačije inicijalizirati
        self.reset_parameters()

    def reset_parameters(self):
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
            elif isinstance(m, nn.Linear) and m is not self.fc_logits:
                nn.init.kaiming_normal_(m.weight, mode='fan_in', nonlinearity='relu')
                nn.init.constant_(m.bias, 0)
        self.fc_logits.reset_parameters()

    def forward(self, x):
        h = self.conv1(x)
        h = torch.relu(h)
        h = self.pool1(h)

        h = self.conv2(h)
        h = torch.relu(h)
        h = self.pool2(h)

        #h = h.contiguous()
        #h = h.view(h.shape[0], -1)
        h = h.reshape((h.shape[0], -1))
        h = self.fc1(h)
        h = torch.relu(h)
        h = self.fc2(h)
        h = torch.relu(h)
        logits = self.fc_logits(h)

        return logits


    def draw_conv_filters(self, epoch, step, weights, save_dir):
        w = weights.cpu().detach().numpy().copy()

        num_filters = w.shape[0]
        num_channels = w.shape[1]
        k = w.shape[2]
        assert w.shape[3] == w.shape[2]
        w = w.transpose(2, 3, 1, 0)
        w -= w.min()
        w /= w.max()
        border = 1
        cols = 8
        rows = math.ceil(num_filters / cols)
        width = cols * k + (cols - 1) * border
        height = rows * k + (rows - 1) * border
        img = np.zeros([height, width, num_channels])
        for i in range(num_filters):
            r = int(i / cols) * (k + border)
            c = int(i % cols) * (k + border)
            img[r:r + k, c:c + k, :] = w[:, :, :, i]
        filename = 'epoch_%02d_step_%06d.png' % (epoch, step)
        ski.io.imsave(os.path.join(save_dir, filename), img)

    def eval_model(self, name, x, y, valid_y_orig, batch_size, get_loss_contrib=False):
        print("\nRunning evaluation: ", name)
        num_examples = x.shape[0]
        assert num_examples % batch_size == 0
        num_batches = num_examples // batch_size
        cnt_correct = 0
        loss_avg = 0

        loss_data_list = []
        yp_list = []

        for i in range(num_batches):
            batch_x = x[i * batch_size:(i + 1) * batch_size, :]
            batch_y = y[i * batch_size:(i + 1) * batch_size, :]
            logits = self.forward(batch_x)

            yp = torch.argmax(logits, 1)
            yt = torch.argmax(batch_y, 1)
            cnt_correct += (yp == yt).sum()


            loss = nn.CrossEntropyLoss()
            loss_val = loss(logits, valid_y_orig[i * batch_size:(i + 1) * batch_size])
            loss_avg += loss_val.detach().item()

            if get_loss_contrib:
                yp_list.append(yp)
                loss_data = nn.CrossEntropyLoss(reduction="none")
                loss_data_list.append(loss_data(logits, valid_y_orig[i * batch_size:(i + 1) * batch_size]))

        if get_loss_contrib:
            loss_data_contrib_list = torch.cat(loss_data_list, dim=0)
            y_preds_list = torch.cat(yp_list, dim=0)

        valid_acc = cnt_correct / num_examples * 100
        loss_avg /= num_batches
        print(name + " accuracy = %.2f" % valid_acc)
        print(name + " avg loss = %.2f\n" % loss_avg)

        del logits
        del loss_val
        del cnt_correct
        del yp
        del yt
        torch.cuda.empty_cache()

        if not get_loss_contrib:
            return valid_acc, loss_avg
        else:
            take_inds = np.argsort(loss_data_contrib_list.cpu().detach().numpy())[::-1]
            return valid_acc, loss_avg, zip(y_preds_list.cpu().detach().numpy()[take_inds,...], valid_y_orig.cpu().detach().numpy()[take_inds,...], x.cpu().detach().numpy()[take_inds,...])

    def train_model(self, train_x, train_y, valid_x, valid_y, train_y_orig, valid_y_orig, config):
        batch_size = config['batch_size']
        max_epochs = config['max_epochs']
        save_dir = config['save_dir']
        init_lr = config['lr']
        lr_gamma = config['lr_gamma']

        N_train, N_val = len(train_x), len(valid_x)

        self.draw_conv_filters(0, 0, self.conv1.weight, save_dir)

        params_for_reg = [par for par in self.parameters()][:-2]
        params_not_for_reg = [par for par in self.parameters()][-2:]
        optimizer = SGD(params=[{"params" : params_for_reg, "weight_decay" : config['weight_decay']},
        {"params" : params_not_for_reg}], lr=init_lr)
        scheduler = ExponentialLR(optimizer, gamma=lr_gamma)


        num_examples = train_x.shape[0]
        assert num_examples % batch_size == 0
        num_batches = num_examples // batch_size

        train_conf_mat_list, train_conf_mats_by_cls_list, train_accuracy_score_list, train_accuracy_avg_list, train_precision_avg_list, train_recall_avg_list, train_accuracy_by_cls_list, train_precision_by_cls_list, train_recall_by_cls_list = [],[],[],[],[],[],[],[],[]
        val_conf_mat_list, val_conf_mats_by_cls_list, val_accuracy_score_list, val_accuracy_avg_list, val_precision_avg_list, val_recall_avg_list, val_accuracy_by_cls_list, val_precision_by_cls_list, val_recall_by_cls_list = [], [], [], [], [], [], [], [], []

        train_loss_list, val_loss_list = [], []
        train_acc_list, val_acc_list = [], []
        lr_list = []

        C = 10
        for epoch in range(1, max_epochs + 1):
            print()
            print("######################## EPOCH", epoch, " ########################")

            lr_list.append(scheduler.get_lr())

            #torch.cuda.empty_cache()

            #train_inds = torch.randperm(num_examples)
            # train_x, train_y = train_x[train_inds], train_y[train_inds]
            # train_y_orig = train_y_orig[train_inds]
            train_x, train_y, train_y_orig = shuffle_data(train_x, train_y, train_y_orig)

            train_x_tens, train_y_tens, train_y_orig_tens = torch.FloatTensor(train_x).cuda(), torch.FloatTensor(train_y).cuda(), torch.LongTensor(train_y_orig).cuda()
            valid_x_tens, valid_y_tens, valid_y_orig_tens = torch.FloatTensor(valid_x).cuda(), torch.FloatTensor(valid_y).cuda(), torch.LongTensor(valid_y_orig).cuda()


            # train
            print("********* TRAIN *********")

            cnt_correct = 0
            train_loss = 0
            for i in range(num_batches):
                batch_x = train_x_tens[i * batch_size:(i + 1) * batch_size, :]
                batch_y = train_y_tens[i * batch_size:(i + 1) * batch_size, :]

                logits = self.forward(batch_x)


                loss = nn.CrossEntropyLoss()
                loss_val = loss(logits, train_y_orig_tens[i * batch_size:(i + 1) * batch_size])

                train_loss += loss_val.detach().item()

                loss_val.backward()
                optimizer.step()
                optimizer.zero_grad()

                logits = logits.detach()
                loss_val = loss_val.detach()

                yp = torch.argmax(logits, 1)
                yt = torch.argmax(batch_y, 1)
                cnt_correct += (yp == yt).sum()

                if i % 100 == 0:
                    print("epoch %d, step %d/%d, batch loss = %.2f" % (epoch, i * batch_size, num_examples, loss_val))

            train_loss /= num_batches
            train_acc = cnt_correct / num_examples * 100

            train_loss_list.append(train_loss)
            train_acc_list.append(train_acc)

            self.draw_conv_filters(epoch, num_batches, self.conv1.weight, save_dir)


            print("Train loss = %.2f" % (train_loss))
            print("Train accuracy = %.2f" % (train_acc))

            self.eval()
            # val
            print()
            print("********* VAL *********")
            with torch.no_grad():
                valid_acc, valid_loss = self.eval_model("Validation", valid_x_tens, valid_y_tens, valid_y_orig_tens, batch_size)

                val_loss_list.append(valid_loss)
                val_acc_list.append(valid_acc)

            conf_mat, conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = evaluate(self, train_x_tens, train_y_orig_tens, N_train, C, batch_size)
            print("TRAIN --metrics")
            print("confusion matrix =")
            print(conf_mat)
            print("confusion matrix by class =")
            print(conf_mats_by_cls)
            print("accuracy score =", accuracy_score)
            print("accuracy avg =", accuracy_avg)
            print("precision avg =", precision_avg)
            print("recall avg =", recall_avg)
            print("accuracy by class =", accuracy_by_cls)
            print("precision by class =", precision_by_cls)
            print("recall by class =", recall_by_cls)

            train_conf_mat_list.append(conf_mat)
            train_conf_mats_by_cls_list.append(conf_mats_by_cls)
            train_accuracy_score_list.append(accuracy_score)
            train_accuracy_avg_list.append(accuracy_avg)
            train_precision_avg_list.append(precision_avg)
            train_recall_avg_list.append(recall_avg)
            train_accuracy_by_cls_list.append(accuracy_by_cls)
            train_precision_by_cls_list.append(precision_by_cls)
            train_recall_by_cls_list.append(recall_by_cls)

            conf_mat, conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = evaluate(self, valid_x_tens, valid_y_orig_tens, N_val, C, batch_size)
            print("VAL --metrics")
            print("confusion matrix =")
            print(conf_mat)
            print("confusion matrix by class =")
            print(conf_mats_by_cls)
            print("accuracy score =", accuracy_score)
            print("accuracy avg =", accuracy_avg)
            print("precision avg =", precision_avg)
            print("recall avg =", recall_avg)
            print("accuracy by class =", accuracy_by_cls)
            print("precision by class =", precision_by_cls)
            print("recall by class =", recall_by_cls)

            val_conf_mat_list.append(conf_mat)
            val_conf_mats_by_cls_list.append(conf_mats_by_cls)
            val_accuracy_score_list.append(accuracy_score)
            val_accuracy_avg_list.append(accuracy_avg)
            val_precision_avg_list.append(precision_avg)
            val_recall_avg_list.append(recall_avg)
            val_accuracy_by_cls_list.append(accuracy_by_cls)
            val_precision_by_cls_list.append(precision_by_cls)
            val_recall_by_cls_list.append(recall_by_cls)

            self.train()

            del logits
            del loss_val
            del cnt_correct
            del train_acc
            del valid_acc
            del yp
            del yt
            del train_x_tens
            del train_y_tens
            del train_y_orig_tens
            del valid_x_tens
            del valid_y_tens
            del valid_y_orig_tens
            del batch_x
            del batch_y
            torch.cuda.empty_cache()

            scheduler.step()

        np.savetxt(config['save_lr_dir'], np.array(lr_list))

        np.savetxt(config['save_train_loss_dir'], np.array(train_loss_list))
        np.savetxt(config['save_val_loss_dir'], np.array(val_loss_list))

        for m in range(len(train_conf_mat_list)):
            np.savetxt(config['save_train_conf_mat_dir'](epoch), train_conf_mat_list[m].reshape(C,C))
            np.savetxt(config['save_val_conf_mat_dir'](epoch), val_conf_mat_list[m].reshape(C, C))

        np.savetxt(config['save_train_acc_dir'], np.array(train_accuracy_avg_list))
        np.savetxt(config['save_val_acc_dir'], np.array(val_accuracy_avg_list))

        np.savetxt(config['save_train_prec_dir'], np.array(train_precision_avg_list))
        np.savetxt(config['save_val_prec_dir'], np.array(val_precision_avg_list))

        np.savetxt(config['save_train_rec_dir'], np.array(train_recall_avg_list))
        np.savetxt(config['save_val_rec_dir'], np.array(val_recall_avg_list))

        np.savetxt(config['save_train_acc_by_cls_dir'], np.array(train_accuracy_by_cls_list))
        np.savetxt(config['save_val_acc_by_cls_dir'], np.array(val_accuracy_by_cls_list))

        np.savetxt(config['save_train_prec_by_cls_dir'], np.array(train_precision_by_cls_list))
        np.savetxt(config['save_val_prec_by_cls_dir'], np.array(val_precision_by_cls_list))

        np.savetxt(config['save_train_rec_by_cls_dir'], np.array(train_recall_by_cls_list))
        np.savetxt(config['save_val_rec_by_cls_dir'], np.array(val_recall_by_cls_list))


        return train_loss_list, train_acc_list, val_loss_list, val_acc_list, lr_list

conv_net = ConvolutionalModel().cuda()
train_loss_list, train_acc_list, val_loss_list, val_acc_list, lr_list = conv_net.train_model(train_x, train_y, valid_x, valid_y, train_y_orig, valid_y_orig, config)

plt.plot(range(1, config['max_epochs']+1), train_loss_list, label="Train")
plt.plot(range(1, config['max_epochs']+1), val_loss_list, label="Val")
plt.legend(loc="best")
plt.title("Cross-entropy loss")
plt.xlabel("n iter")
plt.ylabel("loss")
plt.show()

plt.plot(range(1, config['max_epochs']+1), train_acc_list, label="Train")
plt.plot(range(1, config['max_epochs']+1), val_acc_list, label="Val")
plt.legend(loc="best")
plt.title("Average class accuracy")
plt.xlabel("n iter")
plt.ylabel("accuracy")
plt.show()

plt.plot(range(1, config['max_epochs']+1), lr_list, label="learning rate")
plt.legend(loc="best")
plt.title("Learning rate")
plt.xlabel("n iter")
plt.ylabel("lr")
plt.show()

print()
print("****************************** TEST ******************************")


conv_net.eval()
test_x_tens, test_y_tens, test_y_orig_tens = torch.FloatTensor(test_x).cuda(), torch.FloatTensor(test_y).cuda(), torch.LongTensor(test_y_orig).cuda()
with torch.no_grad():
    _, _, yp_yt_x_zip = conv_net.eval_model("Test", test_x_tens, test_y_tens, test_y_orig_tens, config['batch_size'], get_loss_contrib=True)

    print()
    print("20 incorrectly classified images with biggest loss:")

    di = 0
    num_show_worst = 20
    correct_predicted = dict(zip(range(10), [0 for _ in range(10)]))
    class_freq = dict(zip(range(10), [0 for _ in range(10)]))
    for yp_i, yt_i, xi in yp_yt_x_zip:
        class_freq[yt_i] = class_freq.get(yt_i, 0) + 1

        if (yp_i == yt_i):
            correct_predicted[yt_i] = correct_predicted.get(yt_i, 0) + 1
        else:
            if (di == num_show_worst):
                continue

            di += 1
            plt.title("{}. (p = {}, gt = {})".format(di, NUM_TO_NAME_CLASS_DICT[yp_i], NUM_TO_NAME_CLASS_DICT[yt_i]))

            draw_image(xi, data_mean, data_std, di, NUM_TO_NAME_CLASS_DICT[yp_i], NUM_TO_NAME_CLASS_DICT[yt_i])
            plt.show()


    print()
    class_prob_dict = {}
    num_highest_prob = 3
    for i in range(10):
        class_prob_dict[i] = correct_predicted[i] / class_freq[i]
    print("3 classes with highest probability:")
    for c in dict(sorted(class_prob_dict.items(), key=lambda kv: kv[1], reverse=True)[:num_highest_prob]):
        print("class {} : p = {}".format(NUM_TO_NAME_CLASS_DICT[c], class_prob_dict[c]))





conf_mat, conf_mats_by_cls, accuracy_score, accuracy_avg, precision_avg, recall_avg, accuracy_by_cls, precision_by_cls, recall_by_cls = evaluate(conv_net, test_x_tens, test_y_orig_tens, len(test_x), 10, config['batch_size'])
print("TEST --metrics")
print("confusion matrix =")
print(conf_mat)
print("confusion matrix by class =")
print(conf_mats_by_cls)
print("accuracy score =", accuracy_score)
print("accuracy avg =", accuracy_avg)
print("precision avg =", precision_avg)
print("recall avg =", recall_avg)
print("accuracy by class =", accuracy_by_cls)
print("precision by class =", precision_by_cls)
print("recall by class =", recall_by_cls)