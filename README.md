Convolutional Models. Tasks:

1 Fully connected layer, ReLu nonlinearity and softmax cross entropy with logits loss function

2 L2 regularization

3 Equivalent model in PyTorch

4 Classification on CIFAR-10 dataset

Implemented in Python using NumPy, PyTorch, Matplotlib, skimage and scikit-learn library.

My lab assignment in Deep Learning, FER, Zagreb.

Created: 2021
